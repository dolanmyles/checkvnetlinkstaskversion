﻿using CommandLine;

namespace BFTaskLibrary.Options
{
    [Verb("new-jira-issue", HelpText = "Create a new issue in JIRA")]
    public class JiraOptions : CommonOptions, IJiraOptions
    {
        public string Description { get; set; }
        
        public string Reporter { get; set; }

        public string Summary { get; set; }

    }
}
