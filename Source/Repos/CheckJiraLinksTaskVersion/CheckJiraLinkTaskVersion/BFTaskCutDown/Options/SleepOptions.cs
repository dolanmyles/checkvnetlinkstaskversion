﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using BFTaskProcessor.Options;
using BFTaskLibrary.Options;

using CommandLine;

namespace BFTaskLibrary.Options
//namespace BFTaskLibrary.GeneralTasks
   // BFTaskLibrary.Tasks.GeneralTasks
{
    [Verb("sleep", HelpText = "Sleeps for a specified duration in seconds")]
    public class SleepOptions : CommonOptions
    {
        [Option(Required=true)]
        public int Duration { get; set; }
    }
}
