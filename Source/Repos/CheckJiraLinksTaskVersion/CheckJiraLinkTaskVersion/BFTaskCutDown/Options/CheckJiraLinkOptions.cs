﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;



namespace BFTaskLibrary.Options
{
    [Verb("new-jiralinkcheck", HelpText = "Check JIRA links")]
    public class CheckJiraLinkOptions:CommonOptions

    {

//        [Option('y', Required = false, Default = false)]
  //      public bool DoIt { get; set; }


        [Option('w', Required = false, Default = ".\\")]
        public string VNETOPFilePath { get; set; }


         [Option('p', Required = false, Default = ".\\")]
        public string JiraProjectSecondary { get; set; }


        [Option('k', Required = false, Default = ".\\")]
        public string JiraProjectKeySecondary { get; set; }


        //[Option('l', Required = false, Default = ".\\")]
        //public string LogPath { get; set; }




        public string Description { get; set; }

        public string Reporter { get; set; }

        public string SummaryX { get; set; }


    }
}
