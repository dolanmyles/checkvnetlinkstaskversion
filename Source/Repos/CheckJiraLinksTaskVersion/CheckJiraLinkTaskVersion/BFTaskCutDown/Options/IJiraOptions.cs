namespace BFTaskLibrary.Options
{
    internal interface IJiraOptions
    {
        string Description { get; set; }
        string Reporter { get; set; }
        string Summary { get; set; }
    }
}