﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using CommandLine;
//using NLog.LayoutRenderers;
//using BFTaskLibrary;
using BFTaskLibrary.Attributes;

namespace BFTaskLibrary.Options
{


    public class CommonOptions
    {

        [Option('y', Required = false, Default = false)]
        public bool DoIt { get; set; }

        [Option('l', Required = false, Default = ".\\")]
        public string LogPath { get; set; }


        // Not a specifiable option, but set by the task runner so
        // we can have easy access to the verb. Note this is only
        // guaranteed to be set when the task is executed from the
        // command line.

        public string Verb
        {
            get => _verb;
            set
            {
                if (String.IsNullOrEmpty(_verb))
                {
                    if (String.IsNullOrWhiteSpace(value))
                    {
                        throw new ArgumentException("Verb value cannot be null, empty or whitespace");
                    }
                    _verb = value;
                }
                else
                {
                    throw new ReadOnlyException("Verb can only be set once");
                }
            }
        }

        private string _verb;

        // JIRA options

        [FromEnvironmentVar(Variable = "JIRA_PASSWORD", Required = true)]
        [Option(HelpText = "JIRA User password")]
        public string JiraPassword { get; set; }

        [FromEnvironmentVar(Variable = "JIRA_PROJECT", Required = true)]
        [Option(HelpText = "JIRA project")]
        public string JiraProject { get; set; }

        [FromEnvironmentVar(Variable = "JIRA_PROJECT_KEY", Required = true)]
        [Option(HelpText = "Project Key")]
        public string JiraProjectKey { get; set; }

        [FromEnvironmentVar(Variable = "JIRA_URL", Required = true)]
        [Option(HelpText = "Url for JIRA")]
        public string JiraUrl { get; set; }

        [FromEnvironmentVar(Variable = "JIRA_USER", Required = true)]
        [Option(HelpText = "JIRA User name")]
        public string JiraUsername { get; set; }

        // Google Analytics options
        // Do not change these

        [Option(Default = "c903ec1c-d693-4f09-95d2-37282db675a6")]
        public string GAClientId { get; set; }

        [Option(Default = "UA-63816631-3")]
        public string GAPropertyId { get; set; }


        [Option(Default = false)]
        public bool Tail { get; set; }


    }
}
