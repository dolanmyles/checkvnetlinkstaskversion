﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BFTaskLibrary.Options;




using System.Configuration;
using System.Net.NetworkInformation;

using JIRC;
using JIRC.Domain.Input;
using JIRC.Domain;

using System.IO;
using System.Net;
using NLog;



namespace BFTaskLibrary.Tasks
{
    public class CheckJiraLinkTask<TOptions> : TaskBase<TOptions>
        where TOptions:CheckJiraLinkOptions

    {

        private static IJiraRestClient jiraClient;
        private static string JIRAProjectSecond;
        private static string JIRAUsername;
        private static string JIRAPassword;
        private static string JIRAUrl;
        private static string verb;


      //  public Logger Log2 { get; set; }



        public override void Execute()
        {

            /* 
             * JIRAProject = ConfigurationManager.AppSettings["JIRAProject"];
             JIRAUsername = ConfigurationManager.AppSettings["JIRAUsername"];
             JIRAPassword = ConfigurationManager.AppSettings["JIRAPassword"];
             JIRAUrl = ConfigurationManager.AppSettings["JIRAUrl"];
             jiraClient = JiraRestClientFactory.CreateWithBasicHttpAuth(new Uri(JIRAUrl), JIRAUsername, JIRAPassword);
             */


            JIRAProjectSecond = Options.JiraProjectSecondary;

            JIRAUsername = Options.JiraUsername;
            JIRAPassword = Options.JiraPassword;
            JIRAUrl = Options.JiraUrl;
            jiraClient = JiraRestClientFactory.CreateWithBasicHttpAuth(new Uri(JIRAUrl), JIRAUsername, JIRAPassword);
            verb = Options.Verb;



            var x = Options.VNETOPFilePath;
           // var y = Options.JiraUrl;
            var z = Options.LogPath;

         // CreateJira404("trythis","trythis");

            dosearch();




        }



        public  void dosearch()

        {

            var datelaterthan = Convert.ToDateTime(DateTime.Today).Subtract(TimeSpan.FromDays(200)).ToString("yyyy-MM-dd");

            
            //  DateTime.Today.ToString(("yyyy-mm-dd"));


            File.WriteAllText(@"op.txt", "New Run");
            int j = 0;
            //Various possibilities . ToDo put this query as a command line option
            // string dosearch= "project = VNET AND createdDate > 2015-11-13 order by updated DESC";
            // string dosearch = "project = VNET AND createdDate > 2019-02-13 order by issuekey DESC";
            //string dosearch = "project=VNET AND id = 'VNET-40547' or  id = 'VNET-40543' or  id = 'VNET-40543' or id = 'VNET-38660' ";


               string dosearch = "project = VNET AND createdDate >" +datelaterthan+ " or ( id = 'VNET-40543' OR id = 'VNET-50802' OR id = 'VNET-15010' OR id = 'VNET-51001') order by createdDate DESC";

            //   string dosearch = "project = VNET AND ( id = 'VNET-50802' OR id = 'VNET-15010' OR id = 'VNET-51001')";

          //  string dosearch = "project = VNET AND createdDate > 2019-11-01 or ( id = 'VNET-40543' OR id = 'VNET-50802' OR id = 'VNET-15010' OR id = 'VNET-51001') order by createdDate DESC";



            int maxno = 4000;
            int i = 0;

            //MD SearchClient and SearchJql are methods in interfaces so if you only had a dll you would see their signature params. 
            //var newsearch = jiraClient.SearchClient.SearchJql(dosearch,maxno,i*100).Issues;
            // var newsearch = jiraClient.SearchClient.SearchJql(dosearch, maxno,0).Issues;
            //IEnumerable<Issue> TheseIssues = jiraClient.SearchClient.SearchJql(dosearch).Issues;
            List<string> myCollection = new List<string>();
            IEnumerable<Issue> newsearch;
            var issuecount = 0;
            bool firstlot = true;
            do
            {
                //var newsearch = jiraClient.SearchClient.SearchJql(dosearch,maxno,i*100).Issues;
                newsearch = jiraClient.SearchClient.SearchJql(dosearch, maxno, i * 100).Issues;
                //IEnumerable<Issue> TheseIssues = jiraClient.SearchClient.SearchJql(dosearch).Issues;
                issuecount = newsearch.Count();
                i++;
                firstlot = false;
                string[] urlProbs = null;
           

                //  var newsearch2 = jiraClient.g

                //foreach (var jiraIssue in TheseIssues)
                foreach (var jiraIssue in newsearch)
                {
                    j++;
                    // var x = jiraIssue.CustomFields["customfield_10300"];
                    var pdfurl = jiraIssue.CustomFields["customfield_10200"];


                    //WebRequest request = WebRequest.Create("http://www.contoso.com/default.html");



                    if (pdfurl == null)
                    {
                        File.AppendAllText(@"op.txt", jiraIssue.Key + "Problem!! Probably no PDF url");

                    }


                    else
                    {

                        try
                        {
                            WebRequest request2 = WebRequest.Create(pdfurl.ToString());
                            WebResponse response2 = request2.GetResponse();

                            var responsevar = ((HttpWebResponse)response2).StatusDescription + "  " + j.ToString() + jiraIssue.Key + " "+ j.ToString();
                            var reshead = ((HttpWebResponse)response2).Headers.GetValues("Content-Length")[0] ;
                            var responsevar2 = responsevar + "size" + reshead + Environment.NewLine;
                            int resultreshead = Int32.Parse(reshead);

                            if (resultreshead < 300)
                            {
                                
                                var toosmallvar = jiraIssue.Key + " file too small size is " + reshead + Environment.NewLine;
                                myCollection.Add(toosmallvar);
                                Console.WriteLine(toosmallvar);
                                File.AppendAllText(@"op.txt", toosmallvar);
                            }


                            // Console.WriteLine(((HttpWebResponse)response2).StatusDescription+"  "+j.ToString()+jiraIssue.Key);
                            Console.WriteLine(responsevar2);

                            File.AppendAllText(@"op.txt", responsevar2);



                            //set up elapsed time if ever doing performance checking
                           // Log.Trace($"Result: {Options.Verb}|{responsevar}|{elapsedTimeInMs}");
                            Log.Trace($"Result: {Options.Verb}|{responsevar}");


                            response2.Close();

                        }
                        catch (WebException e)
                        {

                            var exceptionvar = (jiraIssue.Key + $" Exception: '{e}'" ).Substring(0, 100) + Environment.NewLine;
                            // Console.WriteLine($"following exeption: '{e}'");
                            Console.WriteLine(exceptionvar);

                            File.AppendAllText(@"op.txt", exceptionvar);
                            myCollection.Add(exceptionvar);

                            LogException(e);


                        }


                    }
                }

            } while (issuecount > 0 || firstlot == true);

            //  CreateJira2("404s", verb + myCollection.ToString());

            CreateJira404("404s" + verb, string.Concat(myCollection));

        }

        protected void CreateJira404(string summary, string description)
        {

            JiraOptions jiraOptions = new JiraOptions()
            {
                DoIt = Options.DoIt,

                JiraPassword = Options.JiraPassword,
                JiraProject = Options.JiraProject,
                JiraProjectKey = Options.JiraProjectKey,
                JiraUrl = Options.JiraUrl,
                JiraUsername = Options.JiraUsername,

               // Reporter = "myles.dolan@benefacts.ie",
                Summary = summary,
                Description = description

            };

            CreateJiraTask task = new CreateJiraTask()
            {
                IsPartialFail = false,
                Log = Log,
                Options = jiraOptions
            };

            task.Execute();

        }




    }

}


  

