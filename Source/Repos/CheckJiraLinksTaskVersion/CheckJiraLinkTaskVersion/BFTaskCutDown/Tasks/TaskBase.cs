﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using BFTaskLibrary.Options;
using CsvHelper;
using GoogleMeasurementProtocol;
using GoogleMeasurementProtocol.Parameters.ECommerce;
using GoogleMeasurementProtocol.Parameters.EventTracking;
using GoogleMeasurementProtocol.Parameters.Timing;
using GoogleMeasurementProtocol.Parameters.User;
using GoogleMeasurementProtocol.Requests;
using NLog;
using TransactionId = GoogleMeasurementProtocol.Parameters.ECommerce.TransactionId;

namespace BFTaskLibrary
{
    public abstract class TaskBase<TOptions> where TOptions : CommonOptions
    {
        protected TaskBase()
        {
        }
        
        public abstract void Execute();
        public TOptions Options { get; set; }
        public Logger Log { get; set; }

        public int ExitCode = 0;

        public bool IsPartialFail = false;
        public int FailureCount = 0;
        public int SuccessCount = 0;


        protected void LogException(Exception e)
        {
            Log.Error(e);
            Log.Error(e.Message);
            Log.Error(e.StackTrace);
        }

        protected void CreateJira(string summary, string description)
        {

            JiraOptions jiraOptions = new JiraOptions()
            {
                DoIt = Options.DoIt,

                JiraPassword = Options.JiraPassword,
                JiraProject = Options.JiraProject,
                JiraProjectKey = Options.JiraProjectKey,
                JiraUrl = Options.JiraUrl,
                JiraUsername = Options.JiraUsername,

                Reporter = "info",
                Summary = summary,
                Description = description

            };

            CreateJiraTask task = new CreateJiraTask()
            {
                IsPartialFail = false,
                Log = Log,
                Options = jiraOptions
            };

            task.Execute();

        }






        protected void SendGAEvent(string action, string label = null, int? value = null)
        {

            if (!Options.DoIt)
            {
                return;
            }


            try
            {
                GoogleAnalyticsRequestFactory factory = new GoogleAnalyticsRequestFactory(Options.GAPropertyId);
                IGoogleAnalyticsRequest request = factory.CreateRequest(HitTypes.Event);
                request.Parameters.Add(new EventCategory(Options.Verb));
                request.Parameters.Add(new EventAction(action));

                if (label != null)
                {
                    request.Parameters.Add(new EventLabel(label));
                }

                if (value != null)
                {
                    request.Parameters.Add(new EventValue((int)value));
                }

                ClientId clientId = new ClientId(Options.GAClientId);

               request.Post(clientId);

            }
            catch (Exception e)
            {
                Log.Trace($"Failed to send GA event: {Options.Verb}|{action}|{label}|{value}");
                LogException(e);
            }

        }

        protected void SendGATransaction(string transactionId, string item, decimal unitValue, decimal quantity)
        {

            const string eur = "EUR";

            if (!Options.DoIt)
            {
                return;
            }

            try
            {

                ClientId clientId = new ClientId(Options.GAClientId);
                List<Task> tasks = new List<Task>((int)quantity + 1);


                // Post the transaction summary

                GoogleAnalyticsRequestFactory factory = new GoogleAnalyticsRequestFactory(Options.GAPropertyId);
                IGoogleAnalyticsRequest transactionRequest = factory.CreateRequest(HitTypes.Transaction);
                transactionRequest.Parameters.Add(new TransactionId(transactionId));
                transactionRequest.Parameters.Add(new TransactionRevenue(unitValue * quantity));
                
                transactionRequest.Parameters.Add(new GoogleMeasurementProtocol.Parameters.ECommerce.CurrencyCode(eur));
                
                Task transactionTask = transactionRequest.PostAsync(clientId);
                tasks.Add(transactionTask);


                for (int n = 0; n < quantity; n++)
                {
                    IGoogleAnalyticsRequest itemRequest = factory.CreateRequest(HitTypes.Item);

                    itemRequest.Parameters.Add(new TransactionId(transactionId));
                    itemRequest.Parameters.Add(new ItemName(item));
                    itemRequest.Parameters.Add(new ItemPrice(unitValue));
                    transactionRequest.Parameters.Add(new GoogleMeasurementProtocol.Parameters.ECommerce.CurrencyCode(eur));

                    Task itemTask = itemRequest.PostAsync(clientId);
                    tasks.Add(itemTask);
                }

                Task.WaitAll(tasks.ToArray()); 

            }
            catch (Exception e)
            {
                Log.Trace($"Failed to send GA event: {Options.Verb}|{transactionId}|{unitValue}|{quantity}");
                LogException(e);
            }

        }

        protected void SendGATiming(string category, long elapsedTimeInMs, string queueName)
        {
            
            if (!Options.DoIt)
            {
                return;
            }
           

            try
            {
                int timing = Convert.ToInt32(elapsedTimeInMs);


                ClientId clientId = new ClientId(Options.GAClientId);
                GoogleAnalyticsRequestFactory factory = new GoogleAnalyticsRequestFactory(Options.GAPropertyId);
                IGoogleAnalyticsRequest request = factory.CreateRequest(HitTypes.Timing);

                request.Parameters.Add(new UserTimingCategory(category));
                request.Parameters.Add(new UserTimingTime(timing));
                request.Parameters.Add(new UserTimingLabel(queueName));

                request.Post(clientId);

            }
            catch (Exception e)
            {
                Log.Trace($"Failed to send GA timing evnet: {Options.Verb}|{category}|{elapsedTimeInMs}|{queueName}");                
                LogException(e);
            }



        }


        // Statistics helpers

        protected double GetXAsAPercentageOfY(double x, double y)
        {
            return (x * 100) / y;
        }



        protected bool SafeFileCopy(string sourceFileFullPath, string targetFileFullPath, int intervalInSeconds = 4, int maxRetries = 4, bool silent = false)
        {
            bool verbose = !silent;
            if (verbose)
            {
                Log.Info($"Copying {sourceFileFullPath} to {targetFileFullPath}");    
            }
            

            TimeSpan sleepTime = TimeSpan.Zero;
            bool fileCopied = false;

            for (int i = 1; i <= maxRetries && !fileCopied; i++)
            {
                try
                {
                    sleepTime =  TimeSpan.FromSeconds (Math.Pow (intervalInSeconds, i));

                    File.Copy (sourceFileFullPath, targetFileFullPath, true);
                    fileCopied = true;

                    if (verbose)
                    {
                        Log.Info ($"Copy completed: {sourceFileFullPath} to {targetFileFullPath}");    
                    }
                    
                }
                catch (FileNotFoundException)
                {
                    Log.Warn ($"Not found: {sourceFileFullPath}");
                    return false;
                }
                catch (Exception e)
                {
                    Log.Trace(e);
                    Log.Info(e.Message);
                    Log.Info($"Retrying in {sleepTime.Seconds:D} seconds");
                    Thread.Sleep(sleepTime);
                    Log.Info($"Retrying copy to {targetFileFullPath} from {sourceFileFullPath}");
                }
            }

            if (!fileCopied)
            {
                Log.Warn ($"File copied failed after {maxRetries} retries");
            }
            
            return fileCopied;
            
        }

        protected string SerializeToCsv<TEnumberable>(string discriminator, TEnumberable enumberable) where TEnumberable : IEnumerable
        {

            string tempFileName = Path.GetTempFileName ();
            
            Log.Info($"Serialising {discriminator} to {tempFileName}");
            
            using (StreamWriter sw = new StreamWriter(tempFileName))
            {
                var csv = new CsvWriter(sw);
                csv.WriteRecords(enumberable);
            }
            
            return SafeCopyTempFile (discriminator, tempFileName);
        }

        protected string SafeCopyTempFile (string discriminator, string tempFileName)
        {
            string fileName =
                $"{Options.LogPath}\\{Options.Verb}\\{Options.Verb}-{DateTime.Now:yyyy-MM-dd}.{discriminator}.csv";

            if (SafeFileCopy (tempFileName, fileName))
            {
                try
                {
                    File.Delete (tempFileName);
                }
                catch
                {
                    // Does nothing by design
                }
            }

            return fileName;
        }
    }

        
}

