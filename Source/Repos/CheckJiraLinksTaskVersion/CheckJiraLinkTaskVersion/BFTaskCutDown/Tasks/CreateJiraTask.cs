﻿using System;
using System.Linq;
using BFTaskLibrary.Options;
using JIRC;
using JIRC.Domain.Input;

namespace BFTaskLibrary
{
    public class CreateJiraTask : TaskBase<JiraOptions>
    {
        public override void Execute()
        {

            //            Log.Info($"JIRA URL => ${Options.JIRAUrl}");

            _jiraRestClient = JiraRestClientFactory.CreateWithBasicHttpAuth(new Uri(Options.JiraUrl), Options.JiraUsername, Options.JiraPassword);

            var cb = new GetCreateIssueMetadataOptionsBuilder()
                .WithExpandedIssueTypesFields()
                .WithProjectKeys(Options.JiraProjectKey);

            var projectMeta = _jiraRestClient.IssueClient.GetCreateIssueMetadata(cb.Build()).First();
            var taskIssueType = projectMeta.IssueTypes.First(t => t.Name == "Task");

            var ib = new IssueInputBuilder(Options.JiraProjectKey, (long)taskIssueType.Id)
                .SetSummary(Options.Summary ?? "No Summary Specified")
                .SetDescription(Options.Description ?? "No Description Specified");
               // .SetReporterName(Options.Reporter ?? "info");
        

            // TODO: Catch exceptions here and massage them!

            var issueClient = _jiraRestClient.IssueClient.CreateIssue(ib.Build());

        }



        private IJiraRestClient _jiraRestClient;

    }
}
