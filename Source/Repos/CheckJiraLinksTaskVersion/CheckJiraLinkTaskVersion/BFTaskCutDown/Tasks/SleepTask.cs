﻿using System.Threading;
using BFTaskLibrary.Options;

namespace BFTaskLibrary.GeneralTasks
{
    public class SleepTask : TaskBase<SleepOptions>, ISingletonTask
    {
        public override void Execute()
        {
            Log.Info($"Sleeping for {Options.Duration} seconds");
            Thread.Sleep(Options.Duration * 1000);
            Log.Trace($"Slept for {Options.Duration} seconds");
        }
    }
}
