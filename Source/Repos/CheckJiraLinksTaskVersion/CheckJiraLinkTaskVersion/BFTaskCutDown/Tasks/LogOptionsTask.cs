﻿using System.IO;
using BFTaskLibrary.Options;
using NLog;

namespace BFTaskLibrary.GeneralTasks
{
    public class LogOptionsTask<TOptions> : TaskBase<TOptions> where TOptions : CommonOptions
    {
        public override void Execute()
        {
            // If we're in DoIT mode, log the options only to the trace file, otherwise to the console as well
            LogLevel level = LogLevel.Info; // (Options is CommonOptions && (Options as CommonOptions).DoIt) ? LogLevel.Trace : LogLevel.Info;

            Log.Log(level, "Called with options:");
            foreach (var prop in Options.GetType().GetProperties())
            {
                Log.Log(level, $"{prop.Name} = {prop.GetValue(Options)}");
            }

            Log.Log(level, $"CWD = {Directory.GetCurrentDirectory()}");

        }
    }
}
