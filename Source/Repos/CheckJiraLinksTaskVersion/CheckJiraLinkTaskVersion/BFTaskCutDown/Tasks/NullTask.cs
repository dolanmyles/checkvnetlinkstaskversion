﻿using BFTaskLibrary.Options;

namespace BFTaskLibrary.GeneralTasks
{
    public class NullTask<TOptions> : TaskBase<TOptions> where TOptions : CommonOptions
    {
        public override void Execute()
        {
            // Does nothing by design
        }
    }
}
