﻿using BFTaskLibrary.Options;
using CommandLine;

namespace BFTaskLibrary.Tasks.TailTask
{
    [Verb("tail")]
    public class TailTaskOptions : CommonOptions
    {
        [Option(Required = true)]
        public string FileName { get; set; } 
           
        
        [Option(Required = false)]
        public string Title { get; set; }
        
       
    }
}