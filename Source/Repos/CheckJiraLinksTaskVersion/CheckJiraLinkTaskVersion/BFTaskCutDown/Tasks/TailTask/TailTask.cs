﻿using System;
using System.Runtime.Hosting;
using System.Runtime.InteropServices;
using BFTaskLibrary.GeneralTasks;
using BFTaskLibrary.Utility;

namespace BFTaskLibrary.Tasks.TailTask
{
    public class TailTask : TaskBase<TailTaskOptions>, IDisposable
    {
        public override void Execute ()
        {
            if (!Options.Tail)
            {
                return;
            }

            string arguments = String.Empty;

            if (! string.IsNullOrEmpty(Options.Title))
            {
                arguments = $"$host.ui.RawUI.WindowTitle = 'Tail for {Options.Verb}' ; "; 
            }
                
            arguments += $"Get-Content '{Options.FileName}' -Wait";                    
                
            _processHelper = new ProcessHelper("powershell.exe", arguments);
            _processHelper.Start();

        }

        public void Dispose ()
        {
            _processHelper?.Stop();
        }

        private ProcessHelper _processHelper;
    }
}