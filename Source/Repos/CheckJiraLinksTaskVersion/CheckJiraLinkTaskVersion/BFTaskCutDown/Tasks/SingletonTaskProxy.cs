﻿using System;
using System.Diagnostics;
using System.Threading;
using BFTaskLibrary.Options;

namespace BFTaskLibrary.GeneralTasks
{
    public sealed class SingletonTaskProxy<TOptions> : TaskBase<TOptions> where TOptions : CommonOptions
    {

        private readonly TaskBase<TOptions> _target;
        

        public SingletonTaskProxy(TaskBase<TOptions> task)
        {
            _target = task;
        }

        public override void Execute()
        {
            string taskName = _target.GetType().Name;
            Process process = Process.GetCurrentProcess();
            string mutexName = $"{process.ProcessName}-{taskName}";

            bool created = true;
            using (Mutex mutex = new Mutex(true, mutexName, out created))
            {
                if (created)
                {
                    _target.Execute();
                }
                else
                {
                    throw new Exception($"Action {taskName} is already executing");
                }
                
            }

        }

    }
}
