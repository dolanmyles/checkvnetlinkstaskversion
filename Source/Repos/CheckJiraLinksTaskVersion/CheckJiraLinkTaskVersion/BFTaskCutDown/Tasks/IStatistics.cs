﻿namespace BFTaskLibrary.GeneralTasks
{
    public interface IStatistics
    {
        string[] GetStatistics();
    }
}
