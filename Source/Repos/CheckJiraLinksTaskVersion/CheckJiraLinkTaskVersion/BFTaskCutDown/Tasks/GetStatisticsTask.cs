﻿using BFTaskLibrary.Options;

namespace BFTaskLibrary.GeneralTasks
{
    public class GetStatisticsTask<TOptions> : TaskBase<TOptions> where TOptions : CommonOptions
    {

        public GetStatisticsTask(IStatistics statistics)
        {
            _statistics = statistics;
        }

        private readonly IStatistics _statistics;

        public override void Execute()
        {
            string[] stats = _statistics.GetStatistics();
            foreach (string stat in stats)
            {
                Log.Info(stat);
            }
           
        }


    }
}
