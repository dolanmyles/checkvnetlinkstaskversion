﻿using System;
using BFTaskLibrary.Options;

namespace BFTaskLibrary.GeneralTasks
{
    public class PressAnyKeyTask<TOptions> : TaskBase<TOptions> where TOptions : CommonOptions
    {
        public override void Execute()
        {
            Console.WriteLine("Press any key to continuex...");
            Console.ReadKey();
        }
    }
}
