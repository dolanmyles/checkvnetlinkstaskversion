﻿using System;
using System.Collections.Generic;
using System.Reflection;
using BFTaskLibrary.Attributes;
using BFTaskLibrary.GeneralTasks;
using BFTaskLibrary.Options;
using BFTaskLibrary.Tasks.TailTask;
using BFTaskProcessor.Extentions;
using CommandLine;
using GoogleMeasurementProtocol;
using GoogleMeasurementProtocol.Parameters.EventTracking;
using GoogleMeasurementProtocol.Parameters.User;
using GoogleMeasurementProtocol.Requests;
using Newtonsoft.Json.Serialization;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace BFTaskLibrary.Tasks
{
    public static class TaskRunner
    {
        private static FileTarget _traceFileTarget;

        public static int RunTask<TTask, TOptions>(TOptions options)
            where TTask : TaskBase<TOptions>, new() 
            where TOptions : CommonOptions
        {
            options.Verb = typeof(TOptions).GetAttributeValue((VerbAttribute verbAttribute) => verbAttribute.Name);

            Logger taskLogger = CreateLogger(options.Verb, options.LogPath);

            bool optionsSetOk = SetOptionsFromEnvironment (options, taskLogger);
            if (! optionsSetOk)
            {
                return -1;
            }
            

            // Create the verb task and pass in the logger and options

            TaskBase<TOptions> verbTask = new TTask()
            {
                Log = taskLogger,
                Options = options
            };

            // If the concrete task class implements ISingletonTask, proxy the task class
            // with the concrete singleton implementation. This will stop two tasks of the same
            // type trying to execute simultaneously.

            if (verbTask is ISingletonTask)
            {
                verbTask = new SingletonTaskProxy<TOptions>(verbTask);
            }

            // Unless the DoIt option (-y) is specified, pause and ask the user
            // to press a key before executing the verb task.

            TaskBase<TOptions> pressAnyKeyTask = new PressAnyKeyTask<TOptions>(); 
            if (options.DoIt)
            {
                pressAnyKeyTask = new NullTask<TOptions>();
            }

            // If the task implements statistics, get them after the task 
            // has finished executing.

            TaskBase<TOptions> statisticsTask = new NullTask<TOptions>();
            if (verbTask is IStatistics)
            {
                statisticsTask = new GetStatisticsTask<TOptions>(verbTask as IStatistics)
                {
                    Log = taskLogger
                };
            }
            
            // If the Tail option is specified, show the Log

//            TaskBase<TOptions> tailTask = new NullTask<TOptions>();
//            if (options.Tail)
//            {
//                TailTaskOptions _tailTaskOptions = new TailTaskOptions()
//                {
//                    
//                    FileName = _traceFileTarget.FileName.ToString()
//
//                };
//
//                tailTask = new TailTask<TOptions>()
//                {
//                    Log = taskLogger,
//                    Options = _tailTaskOptions
//
//                };
//
//            }

            // Create a list of tasks to execute before/after the verb task.
            // The verb task is included in this list!

            List<TaskBase<TOptions>> tasks = new List<TaskBase<TOptions>>()
            {
                new LogOptionsTask<TOptions>()
                {
                    Log = taskLogger,
                    Options = options
                },
                pressAnyKeyTask,
                verbTask,
                statisticsTask
            };


            // Execute each task. If any excpetion is unhandled (or re-thrown) 
            // by any of the tasks, log and quit indicating an error.

            try
            {
                foreach (TaskBase<TOptions> task in tasks)
                {
                    taskLogger.Trace($"Executing task {task.GetType().Name}");
                    task.Execute();

                    if (task is IDisposable)
                    {
                        (task as IDisposable).Dispose();
                    }
                    
                    
                    taskLogger.Trace($"Task {task.GetType().Name} completed");
                }


                if (verbTask.IsPartialFail)
                {
                    CreateDevOpsJiraIssue(options, taskLogger, true);
                }


            }
            catch (Exception e)
            {
                taskLogger.Error(e);
                taskLogger.Trace(e.StackTrace);
                CreateDevOpsJiraIssue(options, taskLogger, false);
                SendGAExceptionEvent(options, taskLogger, e);
                return -1;
            }

            // The exitCode for success is 0 and this is what
            // the verb task defaults to. If you need a different code then
            // set the verbTask ExitCode property in the Execute() method.

            return verbTask.ExitCode;
        }

        // Options classes passed to CommandLine, may also have FromEnvironmentVariable attributes.
        // This is our own custom attrbiute that lets use specify an environment variable that
        // can contain .
        private static bool SetOptionsFromEnvironment<TOptions> (TOptions options, Logger log)
        {
            bool success = true;
            
            PropertyInfo[] propertyInfo = options.GetType ().GetProperties ();
            foreach (PropertyInfo property in propertyInfo)
            {
                Attribute[] attributes = Attribute.GetCustomAttributes (property);
                foreach (Attribute attribute in attributes)
                {
                    if (!(attribute is FromEnvironmentVarAttribute))
                    {
                        continue;
                    }
                    
                    FromEnvironmentVarAttribute feva = (FromEnvironmentVarAttribute) attribute;
                    string envValue = string.Empty;
                    try
                    {
                        object rawValue = property.GetValue (options);
                            
                        log.Trace($"Property {property.Name} has FEVA attribute. EnvVar = {feva.Variable}");
                        envValue = Environment.GetEnvironmentVariable (feva.Variable);

                        // The implied rule here is that if a command line argument is settable from
                        // the environment, then either the environment variable or the command line optionm
                        // must have a value!
                        
                        // To be settable from the environment, Required must be false (or not set) on the 
                        // Option.
                        
                        if (feva.Required && rawValue == null && string.IsNullOrEmpty (envValue))
                        {
                            log.Error($"Required argument --{property.Name.ToLowerInvariant ()} must be specified on command line or by the environment variable {feva.Variable}");
                            success = false;                    
                            continue;
                        }
                            
                        log.Trace ($"FEVA value = [{envValue}]");
                            
                        string valueToSet = rawValue?.ToString () ?? envValue;
                        log.Info ($"Setting {property.Name} to {valueToSet}");
                        property.SetValue (options, valueToSet);
                            

                    }
                    catch (ArgumentNullException)
                    {
                        // Do nonthing by design

                        log.Info ($"Environment variable {feva.Variable} not found");

                    }
                }
            }

            return success;
        }

        private static void SendGAExceptionEvent(CommonOptions options, Logger log, Exception exception)
        {
            try
            {
                GoogleAnalyticsRequestFactory factory = new GoogleAnalyticsRequestFactory(options.GAPropertyId);
                IGoogleAnalyticsRequest request = factory.CreateRequest(HitTypes.Event);
                request.Parameters.Add(new EventCategory(options.Verb));
                request.Parameters.Add(new EventAction(exception.GetType().ToString()));
                ClientId clientId = new ClientId(options.GAClientId);

                request.Post(clientId);

            }
            catch (Exception)
            {
                log.Trace($"Failed to send GA exception event: {options.Verb}|{exception.GetType()}");
            }

        }

        private static Logger CreateLogger(string verb, string logPath)
        {
            const string consoleLayout = @"${date:format=HH\:mm\:ss} ${message}";
            const string fileTargetLayout = "${date:format=HH\\:mm\\:ss},\"${message}\"";

            LoggingConfiguration logConfig = new LoggingConfiguration();
    
            ColoredConsoleTarget consoleTarget = new ColoredConsoleTarget();
            logConfig.AddTarget("console", consoleTarget);

            _traceFileTarget = new FileTarget();
            logConfig.AddTarget("file", _traceFileTarget);

            consoleTarget.Layout = consoleLayout;
            _traceFileTarget.FileName = $"{logPath}\\{verb}\\{verb}-{DateTime.Now:yyyy-MM-dd}.csv";
            _traceFileTarget.Layout = fileTargetLayout;

            LoggingRule rule1 = new LoggingRule("*", LogLevel.Info, consoleTarget);
            logConfig.LoggingRules.Add(rule1);

            LoggingRule rule2 = new LoggingRule("*", LogLevel.Trace, _traceFileTarget);
            logConfig.LoggingRules.Add(rule2);

            // Errors only log
            const string errorLogName = "errorLog";
            FileTarget errorTarget = new FileTarget();
            logConfig.AddTarget(errorLogName, errorTarget);
            errorTarget.FileName = $"{logPath}\\{verb}\\{verb}-{DateTime.Now:yyyy-MM-dd}.err";
            errorTarget.Layout = fileTargetLayout;
            LoggingRule errorLoggingRule = new LoggingRule(errorLogName, LogLevel.Error, errorTarget);
            logConfig.LoggingRules.Add(errorLoggingRule);

            LogManager.Configuration = logConfig;

            return LogManager.GetLogger("JIRAWatcher");

        }


        private static void CreateDevOpsJiraIssue<TOptions>(TOptions options, Logger log, bool isPartialFail) where TOptions : CommonOptions
        {
            
            // Only create an issue in JIRA if we are running in Jenkins.
            const string jenkinsEnvironmentVar = "JENKINS_HOME";
            string jenkins = Environment.GetEnvironmentVariable (jenkinsEnvironmentVar);
            if (string.IsNullOrEmpty (jenkins))
            {
                log.Trace ($"Jenkins env var {jenkinsEnvironmentVar} not found. Not creating JIRA issue");
                return;
            }
            
            try
            {

                string partially = isPartialFail ? "partially " : "";

                const string buildNumber = "BUILD_NUMBER";
                const string BUILD_URL = "BUILD_URL";

                string buildNum = Environment.GetEnvironmentVariable(buildNumber) ?? $"||{buildNumber}||";
                string buildUrl = Environment.GetEnvironmentVariable(BUILD_URL) ?? $"||{BUILD_URL}||";

                string summary = $"Task \"{options.Verb}\" has {partially}failed";
                string description = $"For details see build {buildNum} log at {buildUrl}";

                JiraOptions jiraOptions = new JiraOptions()
                {
                    DoIt = options.DoIt,

                    JiraPassword = options.JiraPassword,
                    JiraProject = options.JiraProject,
                    JiraProjectKey = options.JiraProjectKey,
                    JiraUrl = options.JiraUrl,
                    JiraUsername = options.JiraUsername,

                    Reporter = "info",
                    Summary = summary,
                    Description = description

                };

                CreateJiraTask task = new CreateJiraTask()
                {
                    IsPartialFail = isPartialFail,
                    Log = log,
                    Options = jiraOptions
                };

                task.Execute();


            }
            catch (Exception e)
            {
                log.Warn("Creating JIRA issue failed.");
                log.Error(e.Message);
                log.Error(e.StackTrace);   
                log.Error(e);

            }
        }
        
        
    }
}