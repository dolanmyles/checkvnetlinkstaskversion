﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace BFTaskProcessor.Extentions
{
    public static class StringExtentions
    {

        public static string ToBase64 (this string str)
        {

            byte[] base64 = Encoding.Default.GetBytes (str);
            string valueAsBase64 = Convert.ToBase64String (base64);

            return valueAsBase64;

        }
        
        
    }
}