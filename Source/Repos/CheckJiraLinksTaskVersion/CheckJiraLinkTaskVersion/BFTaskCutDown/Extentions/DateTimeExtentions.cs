﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace BFTaskLibrary.Extentions
{
    public static class DateTimeExtentions
    {

        public static DateTime FromJiraDateTime (this DateTime dt, string value)
        {
            if (DateTime.TryParseExact (value, "MM/dd/yyyy H:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None,
                out dt))
            {
                return dt;
            }

            return DateTime.MinValue;

        }


        public static int Quater (this DateTime dt)
        {
            
            switch (dt.Month)
            {
                case 1:
                case 2:
                case 3:
                    return 1;
                    
                case 4:    
                case 5:    
                case 6:
                    return 2;
                    
                case 7:    
                case 8:    
                case 9:
                    return 3;
                    
                case 10:    
                case 11:    
                case 12:
                    return 4;
            }

            return -1;
            
        }
        
        
        
        
    }
}