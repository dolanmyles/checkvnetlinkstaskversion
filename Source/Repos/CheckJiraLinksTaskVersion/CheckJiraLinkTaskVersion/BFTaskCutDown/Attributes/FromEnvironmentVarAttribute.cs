﻿using System;

namespace BFTaskLibrary.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class FromEnvironmentVarAttribute : Attribute
    {
        public string Variable { get; set; }
        
        public bool Required = false;

    }
}