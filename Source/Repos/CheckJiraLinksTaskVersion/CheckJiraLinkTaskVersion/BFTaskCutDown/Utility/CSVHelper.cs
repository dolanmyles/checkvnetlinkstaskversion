﻿using System.Collections.Generic;
using System.IO;
using CsvHelper;

namespace BFTaskLibrary.Utility
{
    public class CSVHelper<T>
    {

        public static IEnumerable<T> Load (string fileName)
        {
            
            List<T> list = new List<T> ();

            using (StreamReader reader = new StreamReader (fileName))
            {
                var csv = new CsvReader (reader);

                IEnumerable<T> records = csv.GetRecords<T> ();
                list.AddRange (records);
            }

            return list;

        } 
        
    }
}