﻿using System.Diagnostics;

namespace BFTaskLibrary.Utility
{
    public class ProcessHelper
    {
        private readonly string _arguments;

        private readonly string _name;

        private Process _process;

        public ProcessHelper (string name, string arguments)
        {
            _name = name;
            _arguments = arguments;
        }

        public void Start ()
        {
            var psi = new ProcessStartInfo(_name, _arguments);
            psi.WindowStyle = ProcessWindowStyle.Normal;
            _process = Process.Start(psi);
            
        }


        public void Stop ()
        {
            _process.Close();
        }
    }
}