﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BFTaskCutDown;
using CommandLine;
using BFTaskLibrary.Tasks;
using BFTaskLibrary.Options;
using BFTaskLibrary;

namespace CheckJiraLinkTaskVersion
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> inputArgs = new List<string>();
            for (int i = 0; i < args.GetLength(0); i++)
            {
                inputArgs.Add(args[i]);
            }


            //  var x= CommandLine.Parser.Default.ParseArguments<JiraOptions>;


            int exitCode = CommandLine.Parser.Default.ParseArguments
           <CheckJiraLinkOptions>(args).MapResult(
          (CheckJiraLinkOptions options) => TaskRunner.RunTask<CheckJiraLinkTopTask, CheckJiraLinkOptions>(options),

          
            //   <JiraOptions>(args).MapResult(
            //   (JiraOptions options) => TaskRunner.RunTask<CreateJiraTask, JiraOptions>(options),


            errs => 1
                );

            var x = exitCode;

        //  Console.ReadKey();


        }
    }
}
